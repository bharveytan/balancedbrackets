import isBalanced from './balancedBrackets';

describe('isBalanced', () => {
    describe.each`
    input                                                                                                                                                                         | expectedResult 
    ${''}                                                                                                                                                                         | ${true}
    ${'abc'}                                                                                                                                                                      | ${true}
    ${'('}                                                                                                                                                                        | ${false}
    ${'['}                                                                                                                                                                        | ${false}
    ${'{'}                                                                                                                                                                        | ${false}
    ${')'}                                                                                                                                                                        | ${false}
    ${']'}                                                                                                                                                                        | ${false}
    ${'}'}                                                                                                                                                                        | ${false}
    ${'()'}                                                                                                                                                                       | ${true}
    ${'[]'}                                                                                                                                                                       | ${true}
    ${'{}'}                                                                                                                                                                       | ${true}
    ${'int main() { int x; cin >> x; if (x * (2 + z[1])) { cout << "hi"; } }'}                                                                                                    | ${true}
    ${'({)}'}                                                                                                                                                                     | ${false}
    ${'int main() }'}                                                                                                                                                             | ${false}
    ${'An Ordinary Wizarding Level (often abbreviated O.W.L. is a subject-specific test\ntaken during the fifth year, administrated by the Wizarding Examinations Authority.'}    | ${false}
    `('when input is $input', ({input, expectedResult}) => {
        it(`returns ${expectedResult}`, () => {
            expect(isBalanced(input)).toBe(expectedResult);
        });
    });
});
