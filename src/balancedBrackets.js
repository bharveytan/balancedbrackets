// Write a function that checks the string argument
// for balanced parentheses, square brackets and
// curly braces.

// Balanced:
//   int main() { int x; cin >> x; if (x * (2 + z[1])) { cout << "hi"; } }
// Not balanced:
//   ({)}
// Not balanced:
//   int main() }
// Not balanced:
//   An Ordinary Wizarding Level (often abbreviated O.W.L. is a subject-specific test
//   taken during the fifth year, administrated by the Wizarding Examinations Authority.

// Basically, we distill the string down to just brackets, then start parsing. If we get an opening bracket, push it to the stack.
// If we encounter a closing bracket, we pop the most recent element of the stack out, and see if they match.

export default (str) => {

    const bracketMap = new Map();
    bracketMap.set('{', '}');
    bracketMap.set('(', ')');
    bracketMap.set('[', ']');

    // get rid of everything that is not a bracket
    const bracketArray = str.split('').filter(char => '()[]{}'.indexOf(char) >= 0);
    let bracketStack = [];

    for (const bracketChar of bracketArray) {
        // if it's an open bracket add it to the stack
        if ('([{'.indexOf(bracketChar) >= 0) {
            bracketStack.push(bracketChar);
        } else {
            // if there are orphan closing brackets with no opening bracket return false
            if (bracketStack.length === 0) {
                return false;
            }
            const openingBracket = bracketStack.pop();

            // if the last open bracket doesn't match the current closing bracket return false
            if (bracketMap.get(openingBracket) !== bracketChar) {
                return false;
            }
        }

    }

    return bracketStack.length === 0;
}


